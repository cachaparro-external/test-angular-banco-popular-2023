import { Component, EventEmitter, Input, Output } from '@angular/core';
import Persona from '../model/Persona';

@Component({
  selector: 'app-persona-detail',
  templateUrl: './persona-detail.component.html',
  styleUrls: ['./persona-detail.component.css']
})
export class PersonaDetailComponent {
  @Input()
  person : Persona;

  private _show : boolean;

  display : string;

  @Output()
  carrera : EventEmitter<string>;
  @Output()
  universidad : EventEmitter<string>;

  private _carreraIn : string;
  private _universidadIn : string;

  constructor() {
    this.person = {
      firstName : "",
      lastName : "",
      age: 0
    }

    this._show = false;
    this.display = "none";

    this.carrera = new EventEmitter<string>();
    this.universidad = new EventEmitter<string>();

    this._carreraIn = "";
    this._universidadIn = "";
  }

  fijarDatosEstudio() : void{
    this.carrera.emit("Ingeniería");
    this.universidad.emit("Universidad YYY");
  }

  fijarDatosEstudioVariables(carreraIn : string, universidadIn : string) : void{
    this.carrera.emit(carreraIn);
    this.universidad.emit(universidadIn);
  }

  @Input()
  set show(show : boolean){
    this._show = show;
    this.display = (this._show ? 'block' : 'none');
  }

  get show() : boolean{
    return this._show;
  }

  set carreraIn(carreraIn : string){
    this._carreraIn = carreraIn;
    this.carrera.emit(this._carreraIn);
  }

  get carreraIn() : string{
    return this._carreraIn;
  }

  set universidadIn(universidadIn : string){
    this._universidadIn = universidadIn;
    this.universidad.emit(this._universidadIn);
  }

  get universidadIn() : string{
    return this._universidadIn;
  }
}
