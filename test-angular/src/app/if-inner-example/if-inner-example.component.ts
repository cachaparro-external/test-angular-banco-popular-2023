import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-if-inner-example',
  templateUrl: './if-inner-example.component.html',
  styleUrls: ['./if-inner-example.component.css']
})
export class IfInnerExampleComponent implements OnInit, OnDestroy{

  ngOnInit(): void {
    console.log("Incializando IfInnerExampleComponent");
  }
  ngOnDestroy(): void {
    console.log("Destruyendo IfInnerExampleComponent");
  }

}
