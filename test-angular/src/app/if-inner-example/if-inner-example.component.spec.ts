import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IfInnerExampleComponent } from './if-inner-example.component';

describe('IfInnerExampleComponent', () => {
  let component: IfInnerExampleComponent;
  let fixture: ComponentFixture<IfInnerExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IfInnerExampleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IfInnerExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
