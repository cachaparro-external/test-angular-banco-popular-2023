export default interface Persona{
  firstName : string,
  lastName : string,
  age : number,
  estudiando? : boolean,
  universidad? : string,
  carrera? : string
};
