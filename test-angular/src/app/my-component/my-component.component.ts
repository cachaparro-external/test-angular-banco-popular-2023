import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponentComponent implements OnInit, OnDestroy{

  message : string = "Mensaje de prueba";
  numberValue = 1;
  isDisable = false;
  fontColor = "blue";
  nombre = "";

  ngOnInit() : void{
    console.log("Inicializando el componente MyComponentComponent");
  }

  ngOnDestroy(): void {
    console.log("Destruyendo el componente MyComponentComponent");
  }

  showAlert(){
    alert("Ejecuto la acción");
    this.nombre = "Raul";
    this.fontColor = "green";
    this.isDisable = true;
  };
}
