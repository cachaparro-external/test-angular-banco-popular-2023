import { Component } from '@angular/core';

@Component({
  selector: 'app-switch-example',
  templateUrl: './switch-example.component.html',
  styleUrls: ['./switch-example.component.css']
})
export class SwitchExampleComponent {
  cadenas : string[] = ["A", "B", "C"];
  numeros : number[] = [1, 2, 3, 4];

  cadenaActual : string = this.cadenas[0];
  indice : number = 0;

  moverEnCadenas() : void{
    this.indice++;
    this.indice = this.indice % this.cadenas.length;

    this.cadenaActual = this.cadenas[this.indice];
  }

  moverEnNumeros(){
    this.indice++;
    this.indice = this.indice % 4;
  }
}
