import { Component } from '@angular/core';

@Component({
  selector: 'app-if-example',
  templateUrl: './if-example.component.html',
  styleUrls: ['./if-example.component.css']
})
export class IfExampleComponent {
  show : boolean = false;
  texto : string | undefined = "Valor inicial";
  //texto : string = "";

  /*cambiarTexto() : void{
    if(this.show){
      this.texto = "Mostrando texto TRUE";
    }else{
      this.texto = "Mostrando texto FALSE";
    }

    this.show = !this.show;
  }*/

  cambiarTexto() : void {
    this.show = !this.show;
  }

  /*cambiarTexto() : void {
    this.texto = (this.texto ? undefined : "Otro valor");
  }*/

  /*cambiarTexto() : void {
    this.texto = (this.texto === "Otro valor" ? "Valor inicial" : "Otro valor");
  }*/
}
