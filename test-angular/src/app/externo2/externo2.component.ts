import { Component } from '@angular/core';

@Component({
  selector: 'app-externo2',
  templateUrl: './externo2.component.html',
  styleUrls: ['./externo2.component.css']
})
export class Externo2Component {
  private _primerNombre : string;
  private _primerApellido : string;
  private flag : boolean;

  constructor(){
    this._primerNombre = "";
    this._primerApellido = "";
    this.flag = true;
  }

  cambiarNombre() : void {
    this._primerNombre = (this.flag ? "Cesar" : "Roberto");
    this._primerApellido = (this.flag ? "Cardenas" : "Rodriguez");
    this.flag = !this.flag;
  }

  set primerNombre(primerNombre : string){
    this._primerNombre = primerNombre;
  }

  get primerNombre() : string {
    return this._primerNombre;
  }

  set primerApellido(primerApellido : string){
    this._primerApellido = primerApellido;
  }

  get primerApellido() : string {
    return this._primerApellido;
  }

}
