import { Component } from '@angular/core';
import Persona from '../model/Persona';

@Component({
  selector: 'app-for-example',
  templateUrl: './for-example.component.html',
  styleUrls: ['./for-example.component.css']
})
export class ForExampleComponent {
  names : string[];
  persons : Persona[];
  private newPerson : Persona;

  constructor() {
    this.names = ["Rocio", "Paola", "Lorena"];
    this.persons = [
      {
        firstName: "Rocio",
        lastName: "Ramirez",
        age: 25
      },
      {
        firstName: "Paola",
        lastName: "Perez",
        age: 30
      }
    ];

    this.newPerson = {
      firstName: "",
      lastName: "",
      age: 0
    };
  }

  addPerson() : void {
    this.newPerson = {
      firstName: "Lorena",
      lastName: "Lopez",
      age: 45
    };

    this.persons.push(this.newPerson);
  }
}
