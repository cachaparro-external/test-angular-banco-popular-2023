import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-interno',
  templateUrl: './interno.component.html',
  styleUrls: ['./interno.component.css']
})
export class InternoComponent {
  @Input()
  firstName : string;
  /*@Input("fn")
  firstName : string;*/
  @Input()
  lastName : string;

  constructor(){
    this.firstName = "";
    this.lastName = "";
  }
}
