import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-detail-section',
  templateUrl: './detail-section.component.html',
  styleUrls: ['./detail-section.component.css']
})
export class DetailSectionComponent {
  //Si la propiedad entre el padre y el hijo tienen el mismo nombre
  /*@Input()
  nombre : string = "";*/

  //Si la propiedad enttre el padre y el hijo NO ttienen el mismo nombre
  @Input("nombre")
  name : string = "";

  constructor (){
    this.name = "";
  }
}
