import { Component, OnInit } from '@angular/core';
import { TipoId } from '../model/model';
import { ParamService } from '../service/param.service';

interface Form2Model {
  email : string,
  password : string,
  check: boolean,
  tipoId: number,
  numberId?: string,
  age: number
};

@Component({
  selector: 'app-form2',
  templateUrl: './form2.component.html',
  styleUrls: ['./form2.component.css']
})
export class Form2Component implements OnInit{

  model: Form2Model;
  tiposId: TipoId[];

  constructor(private paramService : ParamService){
    this.tiposId = [];
    this.model = {
      email: "",
      password: "",
      check: true,
      tipoId: 0,
      age: 0
    };
  }

  ngOnInit(): void {
    this.tiposId = this.paramService.getTiposId();
  }

  doSubmit() : void{
    console.log("Email: " + this.model.email);
    console.log("Password: " + this.model.password);
    console.log("Check: " + this.model.check);
    console.log("Tipo ID: " + this.model.tipoId);
  }
}
