import { Injectable } from '@angular/core';
import { TipoId } from '../model/model';

@Injectable({
  providedIn: 'root'
})
export class ParamService {

  constructor() { }

  getTiposId() : Array<TipoId>{
    return [
      {
        id: 1,
        desc: "CC"
      },
      {
        id: 2,
        desc: "TI"
      },
      {
        id: 3,
        desc: "RC"
      },
      {
        id: 4,
        desc: "RC"
      }
    ];
  }
}
