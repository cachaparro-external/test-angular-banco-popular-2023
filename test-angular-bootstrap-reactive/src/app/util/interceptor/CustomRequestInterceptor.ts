import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class CustomRequestInterceptor implements HttpInterceptor{
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log("Interceptando mensaje: " + JSON.stringify(req.body));
    console.log("URL: " + req.url);
    console.log("Header Accept: " + req.headers.get("Accept"));
    console.log("HTTP Method: " + req.method);

    if(req.method != "GET"){
      const requestClone = req.clone({setHeaders: {Authorization: 'Bearer '.concat(environment.TOKEN)}});
      return next.handle(requestClone);
    }else{
      console.log("No agrega header Authorization");
      return next.handle(req);
    }
  }
}

export const CustomRequestInterceptorProvider = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: CustomRequestInterceptor,
    multi: true
  }
];
