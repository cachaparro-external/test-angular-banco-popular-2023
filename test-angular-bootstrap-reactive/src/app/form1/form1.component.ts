import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component {

  //Formulario - Agrupar componentes dentro de un formulario
  form : FormGroup;
  emailControl : FormControl;
  passwordControl : FormControl;
  checkControl : FormControl;

  constructor(){
    this.emailControl = new FormControl("",
      [Validators.required, Validators.minLength(3), Validators.email, Validators.pattern("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")],
      []);
    this.passwordControl = new FormControl("",
      [Validators.required, Validators.minLength(3), Validators.maxLength(10)]);
    this.checkControl = new FormControl(false, []);

    this.form = new FormGroup({
      email: this.emailControl,
      password: this.passwordControl,
      check: this.checkControl
    });
  }

  printValues() : void{
    /*alert("Email: " + this.form.value.email);
    alert("Password: " + this.form.value.password);
    alert("Check: " + this.form.value.check);*/

    alert("Form valido: " + this.form.valid);

    alert("Email: " + this.emailControl.value);
    alert("Password: " + this.passwordControl.value);
    alert("Check: " + this.checkControl.value);
  }

}
