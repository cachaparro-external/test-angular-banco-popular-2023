export interface TipoId{
  id : string,
  description : string,
  status? : number //1 Activo - 0 Inactivo
}
