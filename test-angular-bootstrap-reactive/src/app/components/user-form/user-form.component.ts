import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from 'src/app/model/User';
import { UserService } from 'src/app/service/user.service';
import { User1Service } from 'src/app/service/user1.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit{

  form : FormGroup;
  users : Array<User>;
  user : User;

  constructor(private userService : User1Service,
              private formBuilder : FormBuilder){
    this.users = [];
    //this.users = new Array();

    this.user = {
      id: 0,
      name: "",
      email: "",
      gender: "",
      status: ""
    };

    this.form = this.formBuilder.group({
      id: [""]
    });
  }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe({
      next: (response) => {
        this.users = response;
      },
      error: (error) => {
        alert(error);
      },
      complete: () => {
        console.log("Terminando consulta de usuarios");
      }
    });

    /*this.userService.getUsersByPage(2, 10).subscribe({
      next: (response) => {
        this.users = response;
      },
      error: (error) => {
        alert(error);
      },
      complete: () => {
        console.log("Terminando consulta de usuarios");
      }
    });*/
  }

  /*findUser() : void{
    this.userService.getUserById(this.form.value.id).subscribe({
      next: (response) => {
        this.user = response;
      },
      error: (error) => {
        if(error.status === 404){
          alert("Usuario no existe");
        }else{
          alert("Error: " + JSON.stringify(error));
        }
      }
  });
  }*/


  onClickCreateUser() : void{
    const user : User = {
      name: "Pedro Perez",
      email: "pp12@mail.com",
      gender: "male",
      status: "active"
    };

    this.userService.createUser(user).subscribe({
      next: (response) => {
        console.log("Response: " + JSON.stringify(response));
        alert("El ID del usuario es " + response.id);
      },
      error: (error) => {
        if(error.status === 401){
          alert("Realice nuevamente el proceso de autenticación");
        }else if(error.status === 422){
          alert("El correo electrónico ya existe");
        }else{
          alert("Ocurrio un error");
          console.log(JSON.stringify(error));
        }
      }
    });
  }

  onClickCreateUser1() : void{
    const user : User = {
      name: "Pedro Perez",
      email: "pp7@mail.com",
      gender: "male",
      status: "active"
    };

    this.userService.createUser1(user).subscribe({
      next: (response) => {
        alert(JSON.stringify(response));

        if(response.status === 201){
          alert("El ID del usuario es " + response.body?.id);
          console.log(response.headers.get("Content-Type"));
        }else{
          alert("El usuario ya existe");
        }
      },
      error: (error) => {
        if(error.status === 401){
          alert("Realice nuevamente el proceso de autenticación");
        }else if(error.status === 422){
          alert("El correo electrónico ya existe");
        }else{
          alert("Ocurrio un error");
          console.log(JSON.stringify(error));
        }
      }
    });
  }

  onClickModifyUser() : void{
    const user : User = {
      id: 3494777,
      name: "Pedro Perez",
      email: "pp7@mail.com",
      gender: "male",
      status: "active"
    };

    alert("Ambiente: " + environment.TOKEN);

    this.userService.updateUser(user).subscribe({
      next: (response) => {
        alert(JSON.stringify(response));

        if(response.status === 200){
          alert("El usuario fue modificado de forma correcta");
          console.log(response.headers.get("Content-Type"));
        }else if(response.status === 204){
          alert("El usuario con ID no existe");
        }else{
          alert("Error el código de respuesta es invalido");
        }
      },
      error: (error) => {
        if(error.status === 401){
          alert("Realice nuevamente el proceso de autenticación");
        }else if(error.status === 404){
          alert("El usuario con ID no existe");
        }else if(error.status === 422){
          alert("El correo electrónico ya existe");
        }else{
          alert("Ocurrio un error");
          console.log(JSON.stringify(error));
        }
      }
    });
  }
}
