import { Injectable } from '@angular/core';
import { ListItem } from '../model/ListItem';
import { TipoId } from '../model/TipoId';

@Injectable({
  providedIn: 'root'
})
export class ParamService {

  tiposId : Array<TipoId>;
  tiposIdListItem : Array<ListItem>;

  constructor() {
    this.tiposId = [
      {
        id: "CC",
        description: "Cédula de ciudadania",
        status: 1
      },
      {
        id: "TI",
        description: "Tarjeta de identidad",
        status: 1
      },
      {
        id: "RC",
        description: "Registro Civil",
        status: 1
      },
      {
        id: "PAS",
        description: "Pasaporte",
        status: 0
      }
    ];

    this.tiposIdListItem = new Array();
  }

  /*getTiposId() : Array<ListItem>{
    const listItem : Array<ListItem> = new Array();

    //2. Cache
    //3. Conectarlos a un API
    //4. Crear un validador personalizado
    //5. Crear un  validador async
    /*for(let tipoId of this.tiposId){
      if(tipoId.status === 1){
        listItem.push({
          key: tipoId.id,
          value: tipoId.description
        });
      }
    }*/

    /*this.tiposId
      .filter(tipoId => tipoId.status === 1)
      .map<ListItem>(tipoID => {
        return {
          key: tipoID.id,
          value: tipoID.description
        }
      }).forEach(tipoId => listItem.push(tipoId));

    return listItem;
  }*/

   getTiposId() : Array<ListItem>{
    if(this.tiposIdListItem.length == 0){
      this.tiposId
        .filter(tipoId => tipoId.status === 1)
        .map<ListItem>(tipoID => {
          return {
            key: tipoID.id,
            value: tipoID.description
          }
        }).forEach(tipoId => this.tiposIdListItem.push(tipoId));
    }

    return this.tiposIdListItem;
  }
}
