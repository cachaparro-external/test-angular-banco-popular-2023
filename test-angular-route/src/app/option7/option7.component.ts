import { Component } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-option7',
  templateUrl: './option7.component.html',
  styleUrls: ['./option7.component.css']
})
export class Option7Component {

  private id : string;

  constructor(private router : Router,
              private route : ActivatedRoute
  ) {
    this.id = "100";
  }

  public redirect1() : void{
    //option1
    //this.router.navigate(['/option1']);
    //this.router.navigateByUrl('/option1');

    //option4/100
    //this.router.navigate(['/option4', this.id]);
    //this.router.navigateByUrl('/option4/' + this.id);

    //options?id=200
    //this.router.navigate(['/option5'], {queryParams: {id: "200"}});
    //this.router.navigate(['/option5'], {queryParams: {id: "200"}, fragment: "top"});
    //this.router.navigateByUrl('/option5?id=' + this.id + "#top");

    //../
    //this.router.navigate(['../'], {relativeTo: this.route});
  }

}
