import { NgModule } from '@angular/core';
import { provideRouter, RouterModule, Routes } from '@angular/router';
import { Opcion1Component } from './opcion1/opcion1.component';
import { Opcion2Component } from './opcion2/opcion2.component';
import { Opcion3Component } from './opcion3/opcion3.component';
import { Page404Component } from './page404/page404.component';
import { SecurityPathGuard } from './security/SecurityPathGuard';
import { Option4Component } from './option4/option4.component';
import { Option5Component } from './option5/option5.component';
import { Option6Component } from './option6/option6.component';
import { Option7Component } from './option7/option7.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'option1',
    pathMatch: 'full'
  },
  {
    path: 'option1',
    component: Opcion1Component,
    title: "Option 1"
  },
  {
    path: 'option2',
    component: Opcion2Component,
    title: "Option 2"
  },
  {
    path: 'option3',
    component: Opcion3Component,
    canActivate: [SecurityPathGuard],
    title: "Option 3"
  },
  {
    path: 'option4/:id',
    component: Option4Component,
    title: "Option 4"
  },
  {
    path: 'option5',
    component: Option5Component,
    title: "Option 5"
  },
  {
    path: 'option6',
    component: Option6Component,
    title: "Option 6",
    children: [
      {
        path: 'option61',
        component: Opcion1Component,
      },
      {
        path: 'option62',
        component: Opcion2Component,
      }
    ]
  },
  {
    path: 'redirect',
    component: Option7Component,
    title: "Redirect"
  },
  {
    path: '**',
    component: Page404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
