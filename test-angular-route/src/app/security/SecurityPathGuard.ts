import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";

@Injectable()
export class SecurityPathGuard implements CanActivate{

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean{
    //TODO: Validar lo que sea
    const role : string | null = localStorage.getItem("ROLE");

    if(role !== null){
      if(role === "ADMIN"){
        return true;
      }else{
        return false;
      }
    }

    return false;
  }

}
