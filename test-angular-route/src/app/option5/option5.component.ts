import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-option5',
  templateUrl: './option5.component.html',
  styleUrls: ['./option5.component.css']
})
export class Option5Component {

  @Input() itemId: string | undefined;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.itemId = params['id'];

      console.log("ID: ", this.itemId);
    });
  }

}
