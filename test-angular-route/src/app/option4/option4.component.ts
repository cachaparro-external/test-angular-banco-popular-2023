import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-option4',
  templateUrl: './option4.component.html',
  styleUrls: ['./option4.component.css']
})
export class Option4Component {

  @Input() itemId: string | undefined;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.itemId = params['id'];

      console.log("ID: ", this.itemId);
    });
  }
}
