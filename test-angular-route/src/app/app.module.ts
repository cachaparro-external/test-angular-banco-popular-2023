import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Opcion1Component } from './opcion1/opcion1.component';
import { Opcion2Component } from './opcion2/opcion2.component';
import { Opcion3Component } from './opcion3/opcion3.component';
import { Page404Component } from './page404/page404.component';
import { Option4Component } from './option4/option4.component';
import { Option5Component } from './option5/option5.component';
import { Option6Component } from './option6/option6.component';
import { Option7Component } from './option7/option7.component';

@NgModule({
  declarations: [
    AppComponent,
    Opcion1Component,
    Opcion2Component,
    Opcion3Component,
    Page404Component,
    Option4Component,
    Option5Component,
    Option6Component,
    Option7Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
