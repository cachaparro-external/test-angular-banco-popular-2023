import { Component } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-opcion2',
  templateUrl: './opcion2.component.html',
  styleUrls: ['./opcion2.component.css']
})
export class Opcion2Component {

  constructor(private router : Router){

  }

  goToOption3() : void{
    this.router.navigateByUrl("/option3");
  }

}
