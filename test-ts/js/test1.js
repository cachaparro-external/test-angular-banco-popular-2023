/*console.log("Hola mundo desde JS");

let myVar = 5;

console.log(myVar);

myVar = "Cadena";

console.log(myVar);

function myFunction(){
    let myVar;
};


const myConst = 1;

console.log(myConst);

//myConst = 2;

const myObject = {
    name: "Luis",
    age: 25
};

myObject.name = "Raul";

let otroNumero = 5;
let myValue;

if(otroNumero > 10 && otroNumero < 20){
    myValue = 1;
}else{
    myValue = 2;
}

myValue = (otroNumero > 10 && otroNumero < 20 ? 1 : 2);*/

/*function imprimir(...numeros){
    /*for(let cont = 0; cont < numeros.length; cont++){
        console.log("Numero " + cont, " es: " + numeros[cont]);
    }*/

    /*for(let numero of numeros){
        console.log("Numero " + numero);
    }*/

    /*for(let index in numeros){
        console.log("Numero " + index, " es: " + numeros[index]);
    }
}

imprimir(2, 5, 8, 0);*/

/*const myObject = {
    name: "Luis",
    age: 25,
    birthDate: new Date()
};

for(let field in myObject){
    console.log("Campo " + field + " es: " + myObject[field]);
}*/

/*let myArray = ["Cesar", "Juan", "Luis", "Amanda"];

const [persona1, persona2] = myArray;

const [persona1, ...restoDePersona] = myArray;

const [persona1, , persona2] = myArray;

console.log(persona1);
console.log(persona2);*/

/*function myFunction(cadena){
    console.log(cadena);
};

const myFunction2 = function(cadena){
    console.log(cadena);   
};*/

/*const myFunction3 = (cadena) => {
    console.log(cadena); 
};*/

/*const myFunction3 = cadena => {
    console.log(cadena); 
};*/

/*const myFunction3 = cadena => console.log(cadena);

const myEmptyFunction = () => {
    return null;
};

//1. SOlo tiene 1 parametros, puede omitir el ()
//2 Si solo tiene una linea de ejecucion, se puiede omitir los {}

//myFunction("Valor a imprimir");
//myFunction2("Valor a imprimir");
myFunction3("Valor a imprimir 3");*/

/*const myMap = new Map();

const myMap2 = new Map([
    [1, "Luis"],
    [3, "Ramiro"],
    [20, "Flor"]
]);

myMap.set(1, "Bogota");
myMap.set(12, "Cali");

console.log(myMap);
console.log(myMap2);*/

/*let cityKey = 12;

if(myMap.has(cityKey)){
    console.log(myMap.get(cityKey));
}else{
    console.log("No existe");
}*/

/*myMap2.delete(3);
myMap2.set(20, "Diana");
console.log(myMap2);*/

/*myMap2.forEach((value, key) => {
    console.log(key + ": " + value);
});*/

/*for(const entry of myMap2.entries()){
    console.log(entry[0] + ": " + entry[1]);
}*/

/*for(const key of myMap2.keys()){
    console.log(key + ": " + myMap2.get(key));   
}*/

/*let mySet = new Set();

let mySet2 = new Set([1, 3, 5, 7, 9, 1]);

mySet.add(2);
mySet.add(4);
mySet.add(6);
mySet.add(2);

//console.log(mySet.size);
console.log(mySet);
console.log(mySet2);*/

/*mySet.forEach((value) => {
    console.log(value);
});*/

/*for(const value of mySet.values()){
    console.log(value);
}*/

//has
//delete

/*mySet.clear();

console.log(mySet);*/
/*
let myArray = new Array();
myArray.push(10, 12, 14);

//let otherArray = myArray.map(item => item * 2);

//let todosMayores = myArray.every(item => item > 1);

//let otherArray = myArray.filter(item => item > 10);

//let other = myArray.find(item => item > 10);
//let other = myArray.findIndex(item => item > 10);

/*let valor = myArray.reduceRight((previousValue, currentValue, currentIndex) => {
    return previousValue - currentValue;
});*/

//let otherArray = myArray.slice(0, 1);

//let myArray2 = new Array(1, 2, 3, 2, 1);

/*console.log(myArray);
console.log(otherArray);

/*function myFunction(num1, num2 = 1){
    return num1 * num2;
};

let multiplicacion = myFunction(5, 2);
let multiplicacion1 = myFunction(5);

console.log(multiplicacion);*/

/*let myName = "Pedro";
let myField = "name";

const myObject = {
    [myField]: myName,
    age: 25,
    birthDate: new Date()
};

console.log(myObject);*/

//const myArray = [3, 6, 9, 12];

/*for(const index in myArray){
    myArray[index] = myArray[index] * 2;
};*/

/*const myNewArray = myArray.map((item) => {
    return item * 2;
});*/

//const myNewArray = myArray.map(item => item * 2);

/*const myNewArray = myArray
    .map(item => item * 2)
    .filter(item => item >= 15);

console.log(myArray);
console.log(myNewArray);
*/






function cleanYourClothes(yourclothes, callMeWhenYuuFinish){
    console.log("Lavando: " + yourclothes);
    
    setTimeout(() => {
        callMeWhenYuuFinish(10000);
    }, 5000); 
};

/*function avisarme(){
    console.log("Termine de lavar la ropa");
}*/

//cleanYourClothes("Mi Ropa", avisarme);

cleanYourClothes("Mi Ropa", (cost) => {
    console.log("Termine de lavar la ropa y me debe " + cost);
});

//Normal way
/*function myFunction(name){
    console.log("My name is: " + name);
}

//Annonym
myFunction2 = function(name){
    console.log("My name is " + name);
}

//Arrow
myFunction3 = (name) => {
    console.log("My name is " + name);
};

myFunction("Carlos");
myFunction2("Carlos");
myFunction3("Carlos");*/









