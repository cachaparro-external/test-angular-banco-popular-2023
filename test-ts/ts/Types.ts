type Persona = {
    name: string,
    age: number,
    birthDate: Date
};

interface Persona2{
    name: string,
    age: number,
    birthDate: Date
}

let myPersona : Persona = {name: "Carlos", age: 28, birthDate: new Date()};

console.log(myPersona.name);

let myPersona2 : Persona2 = {
    name: "Carlos",
    age: 28,
    birthDate: new Date()
}