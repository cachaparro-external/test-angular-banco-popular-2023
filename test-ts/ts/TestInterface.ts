interface MyInterface1{
    print() : void;
    sumar(num1 : number, num2 : number) : number;
};

interface Person{
    name : string,
    age : number,
    birthDate : Date,
    gender? : string
};

class MyClass implements MyInterface1{

    public print(): void {
        console.log("Imprimiendo desde MyClass");    
    }

    public sumar(num1 : number, num2 : number) : number{
        return num1 + num2;
    }

    public restar(num1 : number, num2 : number) : number{
        return num1 - num2;
    }
}

let myclass : MyClass = new MyClass();
myclass.print();
console.log(myclass.sumar(5, 2));
console.log(myclass.restar(5, 2));

let myInterface : MyInterface1 = new MyClass();
myInterface.print();
console.log(myInterface.sumar(10, 2));

//Casting
(myInterface as MyClass).restar(2, 1);

/*const person1 : Person = {
    name: "Roberto",
    age: 50,
    birthDate: new Date()
};

console.log(person1.name);

console.log("Edad antes: " + person1.age);
person1.age = 25;

console.log("Edad después: " + person1.age);

const person2 : Person = {
    name: "Roberto",
    age: 50,
    birthDate: new Date(),
    gender: "M"
};*/