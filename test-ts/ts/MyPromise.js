console.log("Ejecutando...");
/*const myPromise : Promise<string> = new Promise(function (resolver, reject){
    let number = Math.floor(Math.random() * 100);
    console.log("Número generado: " + number);

    //Success
    if(number <= 50){
        resolver("Ejecutado exitosamente");
    }else{
        reject({
            code: -1,
            message: "Se presento un error"
        });
    }
});*/
/*const myPromise : Promise<string> = new Promise((resolver, reject) => {
    let number = Math.floor(Math.random() * 100);
    console.log("Número generado: " + number);

    //Success
    if(number <= 50){
        resolver("Ejecutado exitosamente");
    }else{
        reject({
            code: -1,
            message: "Se presento un error"
        });
    }
});*/
var myPromise = new Promise(function (resolver, reject) {
    setTimeout(function () {
        var number = Math.floor(Math.random() * 100);
        console.log("Número generado: " + number);
        //Success
        if (number <= 50) {
            resolver("Ejecutado exitosamente");
        }
        else {
            reject({
                code: -1,
                message: "Se presento un error"
            });
        }
    }, 5000);
});
console.log("Ejecutando promesa...");
/*myPromise
    .then(function(respuesta){
        console.log("La respuesta fue: " + respuesta);
    })
    .catch(function(error){
        console.log(error.code  + " - " + error.message);
    })
    .finally(function(){
        console.log("Finally");
    });*/
myPromise
    .then(function (respuesta) {
    console.log("La respuesta fue: " + respuesta);
})["catch"](function (error) {
    console.log(error.code + " - " + error.message);
})["finally"](function () {
    console.log("Finally");
});
console.log("Ejecutado.");
