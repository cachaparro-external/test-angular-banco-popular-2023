interface Person{
    name : string,
    age : number,
    birthDate : Date,
    gender? : string
};

interface Book{
    name : string,
    pages: number,
    year: number
};

export {Person, Book}