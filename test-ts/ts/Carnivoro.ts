import Animal from "./Animal";

export class Carnivoro extends Animal{
    private name : string;
    
    constructor(name : string){
        super("Carnivoro", 26);

        this.name = name;

        //super.numeroDientes
    }

    public print() : void {
        super.print();
        console.log("Nombre del animal: " + this.name);
    }

    public comer() : void{
        console.log(this.name + " comiendo");
    }
};
