import Animal from "./Animal";
import { Carnivoro } from "./Carnivoro";
import { Book, Person } from "./Data";
import printLoQueSea from "./Generic";

let carnivoro : Carnivoro = new Carnivoro("Perrito");
carnivoro.print();
carnivoro.comer();

let animal : Animal =  new Carnivoro("Gatico");
animal.print();

//Casting
console.log("__________________");
(animal as Carnivoro).comer();

/*const person1 : Person = {
    name: "Roberto",
    age: 50,
    birthDate: new Date()
};

console.log("Person: " + JSON.stringify(person1));

const book1 : Book = {
    name: "El retorno delm rey",
    pages: 500,
    year: 2000
};

console.log("Book " + JSON.stringify(book1));

printLoQueSea("Cadena");
printLoQueSea(1);
printLoQueSea(Date.now());*/
