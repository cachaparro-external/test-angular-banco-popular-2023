/*function printLoQueSea(imprimible : string) : string {
    console.log("Imprimiendo " + imprimible);
    return "Imprimiendo " + imprimible;
};*/

export default function printLoQueSea<T>(imprimible : T) : T {
    console.log("Tipo de Dato: " + typeof imprimible);
    console.log("Imprimiendo " + imprimible);
    return imprimible;
};

/*printLoQueSea("Cadena");
printLoQueSea(1);
printLoQueSea(Date.now());

let myArray : Array<string> = [];
myArray.push("Peras");
myArray.push("Manzanas");
myArray.push(1);*/