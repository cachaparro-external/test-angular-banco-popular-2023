"use strict";
exports.__esModule = true;
var rxjs_1 = require("rxjs");
var myArray = [1, 2, 3];
var myData = (0, rxjs_1.of)("Bogotá", "Medellín", "Calí");
var myData1 = (0, rxjs_1.from)(myArray);
myData.subscribe(function (value) {
    console.log("Ciudad: " + value);
});
myData.subscribe(function (ciudad) {
    console.log("Desde 2 suscriber: " + ciudad);
});
